#!/usr/bin/python3

import os, sys

upstart_fds = int(os.getenv("UPSTART_FDS"))

if not upstart_fds:
    sys.exit(1)

os.dup2(upstart_fds, 3)

os.putenv("LISTEN_FDS", "1")
os.putenv("LISTEN_PID", str(os.getpid()))

os.execvp(sys.argv[1], sys.argv[1:])

sys.exit(1)
